#ifndef THREAD_SAFE_LIST_H_
#define THREAD_SAFE_LIST_H_

/*
 * This list implementation uses fine-grain locking in order to allow thread usage
 * over its elements. List treading safety is maintained using hand-over-hand locking,
 * and element counting safety is kept using a separate counter-lock.
 * This implementation uses the standard pthread library mutex.
 *
 * This list is set to NOT allow duplicates. in order to allow duplicates check the insert member function.
 */

#include <pthread.h>
#include <iostream>
#include <iomanip> // std::setw

using namespace std; //set by TAs, extremely inefficient and unnecessary imo

/*
 * func will try to initialize a lock, and will print an error message (to error channel) if initialization failed
 */
void lock_try_init(pthread_mutex_t *lock) {
    if (pthread_mutex_init(lock, NULL) != 0) {
        std::cerr << "pthread_mutex_init: failed" << std::endl;
        exit(-1);
    }
}

/*
 * func will try to destroy a lock, and will print an error message (to error channel) if destruction failed
 */
void lock_try_destroy (pthread_mutex_t *lock) {
    if (pthread_mutex_destroy(lock) != 0) {
        std::cerr << "pthread_mutex_destroy: failed" << std::endl;
        exit(-1);
    }
}

template <typename T>
class List 
{
    public:
        /**
         * Constructor
         * will create an empty thread-safe-list.
         */
        List() : head(nullptr) , counter(0) {
            lock_try_init(&(this->first_lock));
            lock_try_init(&(this->counter_lock));
        }

        /**
         * Destructor
         * this destructor assumes all usage of the list has halted at the time of destruction.
         */
        ~List(){
            Node *iterator = this->head;
            Node* prev = iterator;
            while (iterator != nullptr) {
                lock_try_destroy(&iterator->node_mutex);
                iterator = iterator->next;
                delete prev;
            }
            lock_try_destroy(&(this->first_lock));
            lock_try_destroy(&(this->counter_lock));
        }

        /**
         * this node subclass is built specifically to allow easy and readable mutex locking and unlocking.
         */
        class Node {
         public:
          T data;
          Node *next;
          pthread_mutex_t node_mutex;

          void node_lock(Node* item) {
              if (item == nullptr) return;
              pthread_mutex_lock(&this->node_mutex);
          }
          void node_unlock(Node* item) {
              if (item == nullptr) return;
              pthread_mutex_unlock(&this->node_mutex);
          }

          Node(const T& data, Node* next=nullptr) : data(data) , next(next) {
              lock_try_init(&this->node_mutex);
          }
          ~Node() {
              lock_try_destroy(&this->node_mutex);
          }
        };


        /**
         * Insert new node to list while keeping the list ordered in an ascending order
         * If there is already a node has the same data as @param data then return false (without adding it again)
         * @param data the new data to be added to the list
         * @return true if a new node was added and false otherwise
         */
        bool insert(const T& data) {
			Node* iterator;
			Node* prev = nullptr;
			Node* first;

			pthread_mutex_lock(&(this->first_lock));
			iterator = this->head;
			first = iterator;

            bool list_empty = (iterator == nullptr);

			iterator->node_lock(iterator);

			/*
			 * this iteration loop will attempt to find the data inside the list.
			 * if the data was not found, prev will hold the new node's father, while iterator will hold its new son.
			 * to reiterate:
			 *                  prev->next = new_node
			 *                  new_node->next = iterator
			 */
			while (!list_empty && iterator->data < data) {
                prev->node_unlock(prev);
                if (prev == first) {
                    pthread_mutex_unlock(&(this->first_lock));
                }
			    prev = iterator;
			    iterator = iterator->next;
			    if (iterator == nullptr)
			        break;
			    iterator->node_lock(iterator);
			}

			//case - data already exists in list. this list does not allow duplicates.
			//if you wish to allow duplicates, feel free to remove this case block. however, further testing is required
			//to insure correctness (pay close attention to node connections set in this function and data deletion,
			//which will probably only delete the first instance of 'data' found).
			if (iterator != nullptr && iterator->data == data) {
                prev->node_unlock(prev);
                iterator->node_unlock(iterator);
                if (iterator == first || prev == first) {
                    pthread_mutex_unlock(&(this->first_lock));
                }
                return false;
            }

			pthread_mutex_lock(&(this->counter_lock));
            this->counter++;
            pthread_mutex_unlock(&(this->counter_lock));
			Node* new_node = new Node(data, iterator);
			if (prev == nullptr) { //either list is empty or the spot to place the node is the first element spot.
			    this->head = new_node;
			    __insert_test_hook(); //ordered by TA to place here
			}
			else {
			    prev->next = new_node;
			    __insert_test_hook(); //ordered by TA to place here
			}

			prev->node_unlock(prev);
            if (prev == first || iterator == first) {
                pthread_mutex_unlock(&(this->first_lock));
            }
			iterator->node_unlock(iterator);
			return true;
        }

        /**
         * Remove the node that its data equals to @param value
         * @param value the data to lookup a node that has the same data to be removed
         * @return true if a matched node was found and removed and false otherwise
         */
        bool remove(const T& value) {
            Node* iterator;
            Node* prev = nullptr;
            Node* first;

            pthread_mutex_lock(&(this->first_lock));
            iterator = this->head;
            first = iterator;
            bool list_empty = (iterator == nullptr);

            iterator->node_lock(iterator);
            /*
             * this iteration loop will attempt to find 'value' arg inside the list.
             */
            while (!list_empty && iterator->data < value) {
                prev->node_unlock(prev);
                if (prev == first)
                    pthread_mutex_unlock(&(this->first_lock));
                prev = iterator;
                iterator = iterator->next;
                if (iterator == nullptr)
                    break;
                iterator->node_lock(iterator);
            }

            /*
             * this 'if' block check whether or not the 'value' was found. if not, a false value will be returned.
             */
            if (iterator == nullptr || iterator->data != value) {
                prev->node_unlock(prev);
                iterator->node_unlock(iterator);
                if (prev == first || iterator == first)
                    pthread_mutex_unlock(&(this->first_lock));
                return false;
            }


            pthread_mutex_lock(&(this->counter_lock));
            this->counter--;
            pthread_mutex_unlock(&(this->counter_lock));
            if (prev == nullptr) { //case - first node
                this->head = iterator->next;
                __remove_test_hook();
            }
            else {
                prev->next = iterator->next;
                __remove_test_hook();
            }



            prev->node_unlock(prev);
            iterator->node_unlock(iterator);
            if (prev == nullptr || prev == first || iterator == first)
                pthread_mutex_unlock(&(this->first_lock));
            delete iterator;
            return true;
        }

        /**
         * Returns the current size of the list
         * @return current size of the list
         */
        unsigned int getSize() {
			pthread_mutex_lock(&(this->counter_lock));
			int size = this->counter;
			pthread_mutex_unlock(&(this->counter_lock));
			return size;
        }

		// Don't remove
        void print() {
          Node* temp = head;
          if (temp == NULL)
          {
            cout << "";
          }
          else if (temp->next == NULL)
          {
            cout << temp->data;
          }
          else
          {
            while (temp != NULL)
            {
              cout << right << setw(3) << temp->data;
              temp = temp->next;
              cout << " ";
            }
          }
          cout << endl;
        }

		// Don't remove
        virtual void __insert_test_hook() {}
		// Don't remove
        virtual void __remove_test_hook() {}



    private:
        Node* head;
        int counter;
        pthread_mutex_t first_lock;
        pthread_mutex_t counter_lock;



};



#endif //THREAD_SAFE_LIST_H_
